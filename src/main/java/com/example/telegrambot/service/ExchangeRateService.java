package com.example.telegrambot.service;

import com.example.telegrambot.client.ExchangeRateClient;

public class ExchangeRateService {

  private final ExchangeRateClient client = new ExchangeRateClient();

  public double getUsdToRub() {
    var exchangeRateResponse = client.getRates("USD", "RUB");
    return exchangeRateResponse.getRates().getRub();
  }

  public double getEurToRub() {
    var exchangeRateResponse = client.getRates("EUR", "RUB");
    return exchangeRateResponse.getRates().getRub();
  }

}
