package com.example.telegrambot.service;

import com.example.telegrambot.client.TelegramClient;

public class TelegramService {

  private TelegramClient client = new TelegramClient();

  public void sendMessage(String message){
    client.sendMessage(message);
  }
}
