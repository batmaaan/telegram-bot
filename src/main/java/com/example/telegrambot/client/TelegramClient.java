package com.example.telegrambot.client;

import com.example.telegrambot.model.telega.TelegramMessageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class TelegramClient {

  private static final String CHAT_ID = "-1001635503439";
  private static final String URL = "https://api.telegram.org/bot5293819881:AAFROar5CPy2ZeAxZwtQbg_p3Q5uNcyn4Xg/sendMessage";
  private final RestTemplate client = new RestTemplate();

  public void sendMessage(String message) {
    var telegramMessageRequest = new TelegramMessageRequest(
      CHAT_ID,
      message
    );

    var requestHttpEntity = new HttpEntity<>(telegramMessageRequest);

    var response = client.postForEntity(
      URL,
      requestHttpEntity,
      Map.class
    );

    // ignore this
    var result = response.getBody();
  }

}
