package com.example.telegrambot.client;

import com.example.telegrambot.model.rates.ExchangeRateResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class ExchangeRateClient {

  private static final String APIKEY = "LKsybmK1xg6n6ktgWRFnBWYVPjn8b4j9";
  private static final String URL = "https://api.apilayer.com/exchangerates_data/latest?";

  private final RestTemplate client = new RestTemplate();

  public ExchangeRateResponse getRates(String from, String to) {

    var initHeaders = new HttpHeaders();
    initHeaders.set("apikey", APIKEY);

    var response = client.exchange(
      URL + "symbols=" + to + "&base=" + from,
      HttpMethod.GET,
      new HttpEntity<>(initHeaders),
      ExchangeRateResponse.class
    );

    return response.getBody();
  }


}
