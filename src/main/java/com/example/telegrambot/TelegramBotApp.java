package com.example.telegrambot;

import com.example.telegrambot.service.ExchangeRateService;
import com.example.telegrambot.service.TelegramService;
import lombok.extern.slf4j.Slf4j;

import java.text.DecimalFormat;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
public class TelegramBotApp {

  private static final ExchangeRateService RATE_SERVICE = new ExchangeRateService();
  private static final TelegramService TELEGRAM_SERVICE = new TelegramService();


  private static final ScheduledThreadPoolExecutor USD_TASK = new ScheduledThreadPoolExecutor(1);
  private static final ScheduledThreadPoolExecutor EUR_TASK = new ScheduledThreadPoolExecutor(1);


  private static double usdToRubCurrent = 0;
  private static double eurToRubCurrent = 0;

  static DecimalFormat df = new DecimalFormat("#.00");
  static double v = 0.5;

  public static void main(String[] args) {

    log.info("initialize telegram bot");

    USD_TASK.scheduleWithFixedDelay(
      () -> {
        var usdToRub = RATE_SERVICE.getUsdToRub();
        if (usdToRubCurrent == 0) {
          log.info("usd changed");

          usdToRubCurrent = usdToRub;
          TELEGRAM_SERVICE.sendMessage("Рубль к доллару = " + df.format(usdToRub));
        }
        if (usdToRubCurrent == usdToRub) {
          log.info("Курс доллара не изменился");
        }
        if (usdToRubCurrent > usdToRub) {
          var math = usdToRubCurrent - usdToRub;
          usdToRubCurrent = usdToRub;

          if (math >= v) {
            TELEGRAM_SERVICE.sendMessage("Доллар Подешевел на " + df.format(math) + " руб!!!" + "Сейчас его цена " + df.format(usdToRub) + " за рубль.");
          } else {
            log.info("Незначительное изменение курса на " + math);
          }
        } else {
          log.info("Евро не упал");
        }
        if (usdToRubCurrent < usdToRub) {
          var math = usdToRub - usdToRubCurrent;
          usdToRubCurrent = usdToRub;
          if (math >= v) {
            TELEGRAM_SERVICE.sendMessage("Доллар подорожал на " + df.format(math) + " руб \uD83D\uDE2D" + " Сейчас его цена " + df.format(usdToRub) + " за рубль.");
          } else {
            log.info("Незначительное изменение курса на " + math);
          }
        } else {
          log.info("Евро не вырос");
        }
      },
      0, 20, TimeUnit.SECONDS
    );

    EUR_TASK.scheduleWithFixedDelay(
      () -> {
        var eurToRub = RATE_SERVICE.getEurToRub();
        if (eurToRubCurrent == 0) {
          log.info("euro changed");
          eurToRubCurrent = eurToRub;
          TELEGRAM_SERVICE.sendMessage("Рубль к евро = " + df.format(eurToRub));
        }
        if (eurToRubCurrent == eurToRub) {
          log.info("Курс евро не изменился");
        }
        if (eurToRubCurrent > eurToRub) {
          var math = eurToRubCurrent - eurToRub;
          eurToRubCurrent = eurToRub;
          if (math >= v) {
            TELEGRAM_SERVICE.sendMessage("Евро Подешевел на " + df.format(math) + " руб!!!" + " Сейчас его цена " + df.format(eurToRub) + " за рубль.");
          } else {
            log.info("Незначительное изменение курса на " + math);
          }
        } else {
          log.info("Евро не упал");
        }
        if (eurToRubCurrent < eurToRub) {
          var math = eurToRub - eurToRubCurrent;
          eurToRubCurrent = eurToRub;
          if (math >= v) {
            TELEGRAM_SERVICE.sendMessage("Евро подорожал на " + df.format(math) + " руб \uD83D\uDE2D" + " Сейчас его цена " + df.format(eurToRub) + " за рубль.");
          } else {
            log.info("Незначительное изменение курса на " + math);
          }
        } else {
          log.info("Евро не вырос");
        }
      },
      0, 20, TimeUnit.SECONDS
    );
  }
}
