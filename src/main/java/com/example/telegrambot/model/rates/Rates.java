package com.example.telegrambot.model.rates;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rates {
  @JsonProperty("RUB")
  private double rub;
  @JsonProperty("USD")
  private double usd;
  @JsonProperty("EUR")
  private double eur;
}
