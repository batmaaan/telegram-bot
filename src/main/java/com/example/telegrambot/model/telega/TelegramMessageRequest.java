package com.example.telegrambot.model.telega;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TelegramMessageRequest {
  private String chat_id;
  private String text;
}
